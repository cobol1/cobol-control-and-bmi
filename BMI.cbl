       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. Son.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  Height      PIC 9(3).
       01  Weight      PIC 9(3)V99.
       01  Score       PIC 9(2)V99.
       01  HeightMeter PIC 9V99.
       PROCEDURE DIVISION .
       Begin.
           DISPLAY "Input height (cm) - " WITH NO ADVANCING 
           ACCEPT Height 
           DISPLAY "Input weight (kg) - " WITH NO ADVANCING 
           ACCEPT Weight 
           
           COMPUTE HeightMeter = Height / 100
           COMPUTE Score = ( Weight / (HeightMeter * HeightMeter))
           DISPLAY "BMI score is " Score 

           IF Score < 18.5   THEN
              DISPLAY "Underweight"
           END-IF

           IF Score >= 18.5 AND <= 22.9 THEN
              DISPLAY  "Normal"
           END-IF 

           IF Score >= 23 AND <= 24.9
              DISPLAY "Risk to Overweight"
           END-IF 

           IF Score >= 25 AND <= 29.9
              DISPLAY "Overweight"
           END-IF

           IF Score >= 30
              DISPLAY "Obese"
           END-IF 
           .
