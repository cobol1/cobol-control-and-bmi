       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Control4.
       AUTHOR. Son.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT StudentFile ASSIGN TO "GRADE.DAT"
            ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION.
       FILE SECTION. 
       FD  StudentFile.
       01  StudentDetails.
           88 EndOfStudentFile  VALUE HIGH-VALUE.
           05 StudentID         PIC X(8).
           05 StudentName       PIC X(16).
           05 CourseCode        PIC X(8).
           05 Grade             PIC X(2).
       PROCEDURE DIVISION .
       Begin.
           OPEN INPUT StudentFile
           
           PERFORM UNTIL EndOfStudentFile 

              READ StudentFile 
              AT END SET EndOfStudentFile TO TRUE
              END-READ
              IF NOT EndOfStudentFile THEN
                 DISPLAY StudentName SPACE StudentID SPACE CourseCode
                    SPACE Grade
               END-IF
           END-PERFORM
           CLOSE StudentFile 
           .