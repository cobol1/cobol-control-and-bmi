       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Control2.
       AUTHOR. Son.

       ENVIRONMENT DIVISION. 
       CONFIGURATION SECTION. 
       SPECIAL-NAMES. 
           CLASS HexNumber IS "0" THRU "9", "A" THRU "F"
           CLASS RealName IS "A" THRU "Z", "a" THRU "z", "'",SPACE.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  NumIn    PIC X(4).
       01  NameIn   PIC X(15).

       PROCEDURE DIVISION.
       Begin.
           DISPLAY "Enter a Hex Number - " WITH NO ADVANCING 
           ACCEPT NumIn.
           IF NumIn IS HexNumber THEN
              DISPLAY NumIn " is a Hex number"
           ElSE
              DISPLAY NumIn " is not a Hex number"
           END-IF

           DISPLAY "__________________________________"
           DISPLAY "Enter a Real name - " WITH NO ADVANCING 
           ACCEPT NameIn.
           IF NameIn IS RealName THEN
              DISPLAY NameIn " is a real name"
           ElSE
              DISPLAY NameIn " is not a real name"
           END-IF

           DISPLAY "__________________________________"
           DISPLAY "Enter a name - " WITH NO ADVANCING 
           ACCEPT NameIn.
           IF NameIn IS ALPHABETIC THEN
              DISPLAY NameIn " is a name"
           ElSE
              DISPLAY NameIn " is not a name"
           END-IF
           .

        